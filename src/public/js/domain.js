var domain = {
  client: {
    fields: {
      company: {label: 'Empresa', type:'text', title:true},
      ref: {label: 'Referência', type:'text'},
      tlf: {label: 'Telefone', type:'text'},
      email: {label: 'Email', type:'email'},
      person: {label: 'Pessoa de contacto', type:'select'},
      address: {label: 'Morada', type:'text'},
      pcode: {label: 'Código postal', type:'text'},
      nif: {label: 'NIF', type:'text'},
      segment: {label: 'Segmento', type:'select'},
      notes: {label: 'Notas', type:'textarea'}
    }
  },

  person: {
    fields: {
        text: {label: 'Nome', type:'text', title:true},
        department: {label: 'Departamento', type:'text'},
        contact: {label: 'Contacto', type:'text'},
        email: {label: 'Email', type:'email'},
        notes: {label: 'Notas', type:'textarea'}
    }
  },

  getFields: function(domain, arrayOfIds){
    var self = this,
        result = {};
    $.each(arrayOfIds, function(i, id){
      result[id] = self[domain].fields[id]
    });
    return result;
  },

  transformToPanelView: function(domain){
    var fields = this[domain].fields;
    for(var field in fields){
      if(fields[field].title){
          fields[field].style = 'panel-title';
          fields[field].label = '';
      }else{
        fields[field].style = 'panel-viewEdit';
      }
    }
    return $.extend(true, {}, fields);
  }

}
