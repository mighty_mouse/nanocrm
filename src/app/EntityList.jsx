import React from 'react';
import NameValueLabel from './NameValueLabel.jsx';

import EntityBox from './EntityBox.jsx';
import FeedbackMessage from './FeedbackMessage.jsx';

/* Contrói a listagem de uma entidade.
** Props:
** data: objecto com dados das entidades
** entity: nome da entidade (client, person, etc)
** fields: array com campos a mostrar (ver domain)
**/
var EntityList = React.createClass({

    getInitialState: function() {
      return {
        data: this.props.data,
        showAsList: false,
        order_by_field: 'id',
        error: false,
        message: ""
      };
    },

    toggleListMode: function() {
      this.setState({showAsList: !this.state.showAsList});
    },

    handleOrderChange: function(e) {
      var field = $(e.target).data('field');
      $.post(this.props.getURL, {order_by_field: field},
        function(data) {
          this.handleSuccess(field, data);
      }.bind(this)).fail(function(data) {
          this.handleFailure(data);
      }.bind(this))
    },

    handleSuccess: function(field, data) {
      if (data) {
          this.setState({data: data, order_by_field: field, error: false});
      } else {
          this.setState({error: true, message: data.msg});
      }
    },

    handleFailure: function(data) {
      this.setState({error: true, message: "Ocorreu um erro. Por favor tente mais tarde"});
    },

    //Clean feedback message
    cleanMessage: function() {
      this.setState({error: false, message: ""});
    },

    render: function() {
        var fields = domain.getFields(this.props.entity, this.props.fields);
        var nodes = [],
            listItems = [];
        var self = this;
        $.each(this.state.data, function(i, dataObj) {
            nodes.push(<EntityBox key={i} entity={dataObj} isPreview={true} showAsList={self.state.showAsList}
                                  deleteURL={controllerURL+"delete"}
                                  fields={fields}/>);
        });
        //By default all entities must have 'id' available to order by
        listItems.push(<li key={'id'}><a href="#" className={this.state.order_by_field == 'id' ? 'active' : ''} data-field="id" onClick={this.handleOrderChange}>Id</a></li>);
        for(var field in fields) {
          if(fields.hasOwnProperty(field)) {
            listItems.push(<li key={field}><a href="#" className={this.state.order_by_field == field ? 'active' : ''} data-field={field} onClick={this.handleOrderChange}>{fields[field].label}</a></li>);
          }
        };
        return (
            <div>
                <FeedbackMessage id="deleteFeedback" success={''} error={this.state.error} message={this.state.message} close={this.cleanMessage}/>
                <div className="row listHeader">
                  <div className="col-xs-12">
                    <div className="row">
                      <div className="dropdown col-xs-10">
                        <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                          Ordenar por...
                          <span className="caret"></span>
                        </button>
                        <ul className="dropdown-menu">
                          {listItems}
                        </ul>
                      </div>
                      <div className="col-xs-2 listToggle">
                        <div className="btn"
                             title={this.state.showAsList ? "Mostrar como grelha" : "Mostrar como lista"}
                             onClick={this.toggleListMode}>
                          <span className={"glyphicon " + (this.state.showAsList ? "glyphicon-th" : "glyphicon-th-list")} aria-hidden="true" ></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {nodes}
                <div className="clearfix"></div>
                <div className="btn-group container-fluid" role="group" aria-label="Criar novo">
                    <a href={controllerURL + "create"} role="button" className="btn btn-primary">Criar novo</a>
                </div>
            </div>
        );
    }
});

export default EntityList;
