import React from 'react';

/* Define o par label/valor. Props:
** fieldObj: o objecto com os dados sobre o campo
** value: o valor a mostrar
**/
var NameValueLabel = React.createClass({

  getInitialState: function() {
    return {value: ''}
  },

  render: function() {
    var value = '';
    switch(this.props.fieldObj.type){
        case 'email':
          value = <a href={"mailto:"+this.props.value}>{this.props.value}</a>;
          break;
        case 'select':
        var self = this;
          $.each(this.props.fieldObj.data, function(i, option){
            if(option.id === self.props.value){
              value = option.text;
            }
          });
          break;
        default:
          value = this.props.value;
          break;
    }
    return(
        <div className={this.props.fieldObj.style ? this.props.fieldObj.style : ''}>
          <label>{this.props.fieldObj.label}</label>
          {this.props.fieldObj.label ? <span>:</span> : ''}
          <span className="labelValue">{value}</span>
        </div>
    )
  }
});

export default NameValueLabel;
