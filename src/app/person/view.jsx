import React from 'react';
import {render} from 'react-dom';

import EntityBox from '../EntityBox.jsx';

class PersonView extends React.Component {
  render () {

    return(
      //'data' is available globally
      <EntityBox entity={data} isPreview={false} fields={domain.transformToPanelView('person')} deleteURL={controllerURL+"delete"} />
    );

  }
}

render(<PersonView/>, document.getElementById('person_view'));
