import React from 'react';
import {render} from 'react-dom';

import EntityList from '../EntityList.jsx';

class PersonIndex extends React.Component {
    render () {
      return(
        //'data' is available globally
        <EntityList data={data} entity={'person'} fields={['text', 'department', 'contact', 'email']} getURL={controllerURL+"get"} />
      )
    }
  }


render(<PersonIndex/>, document.getElementById('person_index'));
