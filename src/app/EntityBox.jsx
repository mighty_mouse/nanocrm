import React from 'react';
import NameValueLabel from './NameValueLabel.jsx';
import Dialog from './Dialog.jsx';
import FeedbackMessage from './FeedbackMessage.jsx';

/* Container para mostrar dados (read-only) de uma entidade.
** Props:
** entity: objecto com dados da entidade
** fields: mapa de campos/labels
** isPreview: booleano para renderizar preview ou painel completo
**/
var EntityBox = React.createClass({

  getInitialState : function() {
      return {showDialog: false, success: false, error: false, message: ""};
  },

  handleClick : function() {
      window.location.href = controllerURL + this.props.entity.id;
  },

  handleEdit : function() {
      window.location.href = controllerURL + 'edit/' + this.props.entity.id;
  },

  handleDelete : function(e) {
    $.post(this.props.deleteURL, {id: this.props.entity.id}, function(data) {
        this.handleSuccess(data);
    }.bind(this)).fail(function(data) {
        this.handleFailure(data);
    }.bind(this))
  },

  handleSuccess: function(data) {
    if (data.id != -1) {
        window.location.href = controllerURL;
    } else {
        this.setState({success: false, error: true, message: data.msg});
    }
  },

  handleFailure: function(data) {
    this.setState({success: false, error: true, message: "Ocorreu um erro. Por favor tente mais tarde"});
  },

  //Clean feedback message
  cleanMessage: function() {
    this.setState({success: false, error: false, message: ""});
  },

  closeDialog: function(){
    this.setState({showDialog: false});
  },

  showDialog: function(e){
    e.preventDefault();
    e.stopPropagation();
    this.setState({showDialog: true});
  },

  handleDialogAction: function(){
    this.handleDelete();
    this.closeDialog();
  },

  render: function() {

    var labels = [];
    for(var field in this.props.fields) {
      labels.push(<NameValueLabel key={''+this.props.entity.id+field}
                                        fieldObj={this.props.fields[field]}
                                        value={this.props.entity[field]} />);
    }
    return(
      <div className={"box " + (this.props.isPreview ? (this.props.showAsList ? "col-xs-12" : "col-md-4 col-sm-6") : "")}>
        <div className={"panel panel-default " + (this.props.isPreview ? "boxLink" : "")}
             onClick={this.props.isPreview ? this.handleClick : undefined}>
          <div className="panel-body">
            {labels}
            {this.props.isPreview
                ? <div className="deleteBtn" onClick={this.showDialog} title="Eliminar">
                      <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                  </div>
                : <div className="btn-group" role="group" aria-label="Editar">
                      <button type="submit" className="btn btn-primary" onClick={this.handleEdit}>Editar</button>
                      <button type="submit" className="btn btn-default" onClick={this.showDialog}>Eliminar</button>
                  </div>
            }
          </div>
        </div>
        <FeedbackMessage id="deleteFeedback" success={this.state.success} error={this.state.error} message={this.state.message} close={this.cleanMessage}/>
        <Dialog id={"dialog_"+this.props.entity.id}
          showButtons={true}
          title={"Confirmar eliminação"}
          content={<p>Deseja eliminar esta entidade?</p>}
          show={this.state.showDialog} close={this.closeDialog} action={this.handleDialogAction} />
      </div>
    );
  }
});

export default EntityBox;
