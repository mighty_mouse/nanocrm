import React from 'react';

import EntityInput from './EntityInput.jsx';
import Dialog from './Dialog.jsx';
import FeedbackMessage from './FeedbackMessage.jsx';

/* Componente de formulário para criação de entidades.
** Props:
** fields: descrição dos campos a apresentar
** submitURL: URL para submissão dos dados
** title: titulo a apresentar
**/
var EntitySet = React.createClass({

    getInitialState : function() {
        var fieldValues = {};
        for (var field in this.props.fields) {
            fieldValues[field] = (this.props.entityData)
                ? this.props.entityData[field]
                : '';
        }
        if (this.props.entityData) {
            fieldValues['id'] = this.props.entityData['id'];
        }
        return {fieldValues: fieldValues, success: false, error: false, message: ""};
    },

    handleChange: function(e) {
        this.state.fieldValues[e.target.id] = e.target.value;
        this.setState(this.state);
        this.cleanMessage();
    },

    handleSubmit: function() {
        $.post(this.props.submitURL, this.state.fieldValues, function(data) {
            this.handleSuccess(data);
        }.bind(this)).fail(function(data) {
            this.handleFailure(data);
        }.bind(this))
    },

    handleSuccess: function(data) {
        if (data.id != -1) {
            this.setState({success: true, error: false, message: data.msg});
        } else {
            this.setState({success: false, error: true, message: data.msg});
        }
        if (this.props.dialog) {
            window.setTimeout(this.props.close, 1500);
            this.cleanMessage();
        }
    },

    handleFailure: function(data) {
        this.setState({success: false, error: true, message: "Ocorreu um erro. Por favor tente mais tarde"});
        if (this.props.dialog) {
            window.setTimeout(this.props.close, 1500);
            this.cleanMessage();
        }
    },

    //Clean feedback message
    cleanMessage: function() {
        this.setState({success: false, error: false, message: ""});
    },

    render: function() {

        var inputFields = [];
        for (var field in this.props.fields) {
            var fieldObj = this.props.fields[field];
            fieldObj.value = this.state.fieldValues[field];
            inputFields.push(
                <div key={field} className={(fieldObj.style
                    ? fieldObj.style
                    : "") + " form-group"}>
                    <label htmlFor={field} className={this.props.dialog
                        ? "col-sm-3"
                        : "col-sm-2" + " control-label"}>{fieldObj.label}</label>
                    <div className={this.props.dialog
                        ? "col-sm-9"
                        : "col-sm-6"}>
                        <EntityInput fieldId={field} fieldObj={fieldObj} style="form-control" handleChange={this.handleChange}/>
                    </div>
                </div>
            );
        }

        return (
            <div className="panel panel-default entityCreate">
                <FeedbackMessage id="createFeedback" success={this.state.success} error={this.state.error} message={this.state.message} close={this.cleanMessage}/>
                <div className="panel-body">
                    <p className="panel-title">{this.props.title}</p>
                    <div className="form-horizontal" role="form">
                        {inputFields}
                        <div className="btn-group" role="group" aria-label="Submeter">
                            <button type="submit" className="btn btn-primary" onClick={this.handleSubmit}>Submeter</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

});

export default EntitySet;
