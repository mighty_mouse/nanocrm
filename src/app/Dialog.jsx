import React from 'react';

/* Caixa de diálogo como ou sem acções. Props:
** id: identificador único
** title: título (cabeçalho)
** content: WYSIWYG
**/
var Dialog = React.createClass({

  getInitialState: function() {
    return {}
  },

  componentWillReceiveProps: function(nextProps){
    if(nextProps.show){
      $('.overlay').show();
    }else{
      $('.overlay').hide();
    }
  },

  handleAction: function() {
    this.props.action();
  },

  render: function() {

    var bottomButtons = this.props.showButtons ?
                        <div className="bottomButtons">
                          <button type="button" className="btn btn-default" onClick={this.props.close}>Fechar</button>
                          <button type="button" className="btn btn-primary pull-right" onClick={this.handleAction}>Confirmar</button>
                        </div>
                        : "";

    return(
        <div id={this.props.id} className={"dialog-container " + (this.props.show ? "" : "hidden")}>
          <button type="button" className="dialogTop close" onClick={this.props.close}><span aria-hidden="true">&times;</span></button>
          <div className="dialog">
            <div className="dialog-header header-title">
              {this.props.title}
            </div>
            <div className="content">
              {React.cloneElement(this.props.content, {close: this.props.close})}
            </div>
            {bottomButtons}
          </div>
        </div>
    );
  }

});

export default Dialog;
