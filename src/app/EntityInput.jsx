import React from 'react';

import Dialog from './Dialog.jsx';

/* Componente de input para uma entidade.
** Props:
** fieldId: identificador único para o elemento
** fieldObj: objecto com os dados para o elemento a criar
**
** style: classes CSS a aplicar ao elemento
**/
var EntityInput = React.createClass({

  getInitialState: function() {
    return {
      showDialog: false,
      value: this.props.fieldObj.value
    }
  },

  handleChange: function(e) {
    this.setState({value: e.target.value});
    this.props.handleChange(e);
  },

  closeDialog: function(){
    this.setState({showDialog: false});
  },

  showDialog: function(){
    this.setState({showDialog: true});
  },

  handleDialogAction: function(){
    //TODO: Actualizar lista 
    this.closeDialog();
  },

  render: function() {

    var fieldInput;
    switch(this.props.fieldObj.type){
      case 'select':
        var options = [];
        options.push(<option key={0} value=''>Seleccione uma opção</option>);
        $.each(this.props.fieldObj.data, function(i, optObj){
          options.push(<option key={i+1} value={optObj.id}>{optObj.text}</option>);
        });
        var selectInput = <select defaultValue=''
                                  className={this.props.style}
                                  id={this.props.fieldId}
                                  name={this.props.fieldId}
                                  onChange={this.props.handleChange} >
                             {options}
                          </select>;
        if(this.props.fieldObj.addEntityData){
          fieldInput =
          <div className="row">
            <div className="col-xs-9">
              {selectInput}
            </div>
            <div className="col-xs-3">
              <button type="button" className="btn btn-default pull-right"
                      onClick={this.showDialog}>Adicionar</button>
            </div>
            <Dialog id={"dialog_"+this.props.fieldId}
                    showButtons={false}
                    title={"Adicionar " + this.props.fieldObj.label.toLowerCase()}
                    content={this.props.fieldObj.addEntityData.content}
                    show={this.state.showDialog} close={this.closeDialog} action={this.handleDialogAction} />
          </div>
        }else{
          fieldInput = selectInput;
        }
        break;
      case 'textarea':
        fieldInput = <textarea className={this.props.style}
                               id={this.props.fieldId}
                               name={this.props.fieldId}
                               placeholder={this.props.fieldObj.label}
                               onChange={this.props.handleChange} />;
        break;
      default:
        fieldInput = <input type={this.props.fieldObj.type}
                            className={this.props.style}
                            id={this.props.fieldId}
                            name={this.props.fieldId}
                            placeholder={this.props.fieldObj.label}
                            onChange={this.handleChange}
                            value={this.state.value} />;
        break;
    }

    return(
     fieldInput
    );
  }

});

export default EntityInput;
