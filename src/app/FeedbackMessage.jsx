import React from 'react';

var FeedbackMessage = React.createClass({

    render: function() {
      var style = "alert alert-dismissible";
      if(this.props.success){
        style += " alert-success";
      } else if(this.props.error){
        style += " alert-danger";
      } else {
        style += " hidden";
      }
      return(
          <div className={style} role="alert">
            <button type="button" className="close" onClick={this.props.close}><span aria-hidden="true">&times;</span></button>
            <span>{this.props.message}</span>
          </div>
      )
    }

});

export default FeedbackMessage;
