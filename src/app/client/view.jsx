import React from 'react';
import {render} from 'react-dom';

import EntityBox from '../EntityBox.jsx';

class ClientView extends React.Component {
  render () {

    var fields = domain.transformToPanelView('client');
    fields.person.data = persons;
    fields.segment.data = segments;

    return(
      //'data' is available globally
      <EntityBox entity={data} isPreview={false} fields={fields} deleteURL={controllerURL+"delete"} />
    );

  }
}

render(<ClientView/>, document.getElementById('client_view'));
