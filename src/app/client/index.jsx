import React from 'react';
import {render} from 'react-dom';

import EntityList from '../EntityList.jsx';

class ClientIndex extends React.Component {

    render () {
      return(
        //'data' is available globally
        <EntityList data={data} entity={'client'} fields={['company', 'ref', 'tlf', 'email']} getURL={controllerURL+"get"} />
      )

    }
  }


render(<ClientIndex/>, document.getElementById('client_index'));
