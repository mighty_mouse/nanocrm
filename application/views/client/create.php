<script type="text/javascript">
  var segments = <?php echo json_encode($segments) ?>;
  var persons = <?php echo json_encode($persons) ?>;
  var controllerURL = "<?php echo site_url('client/') ?>";
</script>

<div id="client_create"></div>

<script src="/src/public/client_create.bundle.js" type="text/javascript"></script>
