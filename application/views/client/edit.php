<script type="text/javascript">
  var data = <?php echo json_encode($client, JSON_FORCE_OBJECT) ?>;
  var segments = <?php echo json_encode($segments) ?>;
  var persons = <?php echo json_encode($persons) ?>;
  var controllerURL = "<?php echo site_url('client/') ?>";
</script>

<div id="client_edit"></div>

<script src="/src/public/client_edit.bundle.js" type="text/javascript"></script>
