<script type="text/javascript">
  var data = <?php echo json_encode($clients, JSON_FORCE_OBJECT) ?>;
  var controllerURL = "<?php echo site_url('client/') ?>";
</script>

<div id="client_index"></div>

<script src="/src/public/client_index.bundle.js" type="text/javascript"></script>
