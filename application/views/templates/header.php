<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>NanoCRM</title>

    <link rel="icon" href="/src/public/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="/src/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/src/public/css/app.css">
    <link rel="stylesheet" href="/src/public/css/dialog.css">
    <script type="text/javascript" src="/src/public/js/jquery-3.1.0.min.js"></script>
    <script type="text/javascript" src="/src/public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/src/public/js/domain.js"></script>
    <script type="text/javascript" src="/src/public/vendors.js" ></script>
    <script type="text/javascript">
      var baseURL = "<?php echo site_url('') ?>";
    </script>
  </head>
  <body>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo site_url(''); ?>">NanoCRM</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           <ul class="nav navbar-nav">
             <li class="<?php echo $entity == 'client' ? 'active' : '' ?>">
               <a href="<?php echo site_url('client'); ?>">Clientes</a>
             </li>
             <li class="<?php echo $entity == 'person' ? 'active' : '' ?>">
               <a href="<?php echo site_url('person'); ?>">Pessoas</a>
             </li>
           </ul>
         </div>
       </div><!-- /.container-fluid -->
    </nav>
    <div class="container-fluid">
