<script type="text/javascript">
  var data = <?php echo json_encode($persons, JSON_FORCE_OBJECT) ?>;
  var controllerURL = "<?php echo site_url('person/') ?>";
</script>

<div id="person_index"></div>

<script src="/src/public/person_index.bundle.js" type="text/javascript"></script>
