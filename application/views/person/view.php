<script type="text/javascript">
  var data = <?php echo json_encode($person, JSON_FORCE_OBJECT) ?>;
  var controllerURL = "<?php echo site_url('person/') ?>";
</script>

<div id="person_view"></div>

<script src="/src/public/person_view.bundle.js" type="text/javascript"></script>
