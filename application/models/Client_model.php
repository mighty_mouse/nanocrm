<?php
class Client_model extends CI_Model {

        public function __construct(){
            $this->load->database();
        }

        public function get_clients($id = FALSE){
            if ($id === FALSE){
                $query = $this->db->get('client');
                return $query->result_array();
            }
            $query = $this->db->get_where('client', array('id' => $id));
            return $query->row_array();
        }

        public function get_clients_by($order_by_field = 'id'){
            try{
              $this->db->from('client');
              $this->db->order_by($order_by_field, 'asc');
              $query = $this->db->get();
              return $query->result_array();
            }catch(Exception $e){
              return array('msg' => 'Ocorreu um erro na base de dados. Por favor tente mais tarde');
            }
        }

        public function set_client(){
            $this->load->helper('url');

            try{
              $data = array(
                  'company' => $this->input->post('company'),
                  'ref' => $this->input->post('ref'),
                  'email' => $this->input->post('email'),
                  'tlf' => $this->input->post('tlf'),
                  'person' => $this->input->post('person'),
                  'address' => $this->input->post('address'),
                  'pcode' => $this->input->post('pcode'),
                  'nif' => $this->input->post('nif'),
                  'segment' => $this->input->post('segment'),
                  'notes' => $this->input->post('notes')
              );
              $id = $this->input->post('id');

              if($id){
                $this->db->where('id', $id);
                $this->db->update('client', $data);
                return array('id' => $id, 'msg' => 'Cliente actualizado com sucesso.');
              }else{
                $this->db->insert('client', $data);
                return array('id' => $this->db->insert_id(), 'msg' => 'Cliente criado com sucesso.');
              }

            }
            catch(Exception $e){
              return array('id' => -1, 'msg' => 'Ocorreu um erro na base de dados. Por favor tente mais tarde');
            }

        }

        public function delete_client(){
            $this->load->helper('url');

            try{
              $id = $this->input->post('id');

              if($id){
                $this->db->where('id', $id);
                $this->db->delete('client');
                return array('id' => $id, 'msg' => 'Cliente eliminado com sucesso.');
              }

            }
            catch(Exception $e){
              return array('id' => -1, 'msg' => 'Ocorreu um erro na base de dados. Por favor tente mais tarde');
            }

        }

}
