<?php
class Person_model extends CI_Model {

        public function __construct(){
            $this->load->database();
        }

        public function get_persons($id = FALSE){
            if ($id === FALSE){
                $query = $this->db->get('person');
                return $query->result_array();
            }
            $query = $this->db->get_where('person', array('id' => $id));
            return $query->row_array();
        }

        public function get_persons_by($order_by_field = 'id'){
            try{
              $this->db->from('person');
              $this->db->order_by($order_by_field, 'asc');
              $query = $this->db->get();
              return $query->result_array();
            }catch(Exception $e){
              return array('msg' => 'Ocorreu um erro na base de dados. Por favor tente mais tarde');
            }
        }

        public function set_person(){
            $this->load->helper('url');

            try{
              $data = array(
                  'text' => $this->input->post('text'),
                  'department' => $this->input->post('department'),
                  'contact' => $this->input->post('contact'),
                  'email' => $this->input->post('email'),
                  'notes' => $this->input->post('notes')
              );
              $id = $this->input->post('id');

              if($id){
                $this->db->where('id', $id);
                $this->db->update('person', $data);
                return array('id' => $id, 'msg' => 'Pessoa actualizada com sucesso.');
              }else{
                $this->db->insert('person', $data);
                return array('id' => $this->db->insert_id(), 'msg' => 'Pessoa criada com sucesso.');
              }
            }
            catch(Exception $e){
              return array('id' => -1, 'msg' => 'Ocorreu um erro na base de dados. Por favor tente mais tarde');
            }

        }

        public function delete_person(){
            $this->load->helper('url');

            try{
              $id = $this->input->post('id');

              if($id){
                $this->db->where('id', $id);
                $this->db->delete('person');
                return array('id' => $id, 'msg' => 'Pessoa eliminada com sucesso.');
              }

            }
            catch(Exception $e){
              return array('id' => -1, 'msg' => 'Ocorreu um erro na base de dados. Por favor tente mais tarde');
            }

        }
}
