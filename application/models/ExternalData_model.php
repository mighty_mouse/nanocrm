<?php
class ExternalData_model extends CI_Model {

        public function __construct(){
            $this->load->database();
        }

        public function get_segments(){
            $query = $this->db->get('segment');
            return $query->result_array();
        }

        public function get_persons(){
            $query = $this->db->get('person');
            return $query->result_array();
        }

}
