<?php
class Person extends CI_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('person_model');
            $this->load->model('externalData_model');
            $this->load->helper('url_helper');
        }

        public function view($id = NULL){
            $data['entity'] = 'person';
            $data['person'] = $this->person_model->get_persons($id);

            if (empty($data['person'])){
                show_404();
            }

            $this->load->view('templates/header', $data);
            $this->load->view('person/view', $data);
            $this->load->view('templates/footer');
        }

        public function index(){
            $data['entity'] = 'person';
            $data['persons'] = $this->person_model->get_persons();
            $data['title'] = 'Pessoas de contacto';

            $this->load->view('templates/header', $data);
            $this->load->view('person/index', $data);
            $this->load->view('templates/footer');
        }

        public function get(){
          $this->output->set_content_type('application/json');
          $response = $this->person_model->get_persons_by($this->input->post('order_by_field'));
          echo json_encode($response);
        }

        public function create(){
            $data['entity'] = 'person';
            $this->load->helper('form');

            $this->load->view('templates/header', $data);
            $this->load->view('person/create');
            $this->load->view('templates/footer');
        }

        public function edit($id = NULL){
            $data['entity'] = 'person';
            $data['person'] = $this->person_model->get_persons($id);

            if (empty($data['person'])){
                show_404();
            }

            $this->load->view('templates/header', $data);
            $this->load->view('person/edit', $data);
            $this->load->view('templates/footer');
        }

        public function submit(){
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('text', 'Nome', 'required');

            $this->output->set_content_type('application/json');

            if ($this->form_validation->run() == FALSE){
              $response = array('id' => -1, 'msg' => 'Campo(s) inválido(s) ou em falta');
            }else{
              $response = $this->person_model->set_person();
            }

            echo json_encode($response);
        }

        public function delete(){
            $this->output->set_content_type('application/json');
            $response = $this->person_model->delete_person();
            echo json_encode($response);
        }
}
