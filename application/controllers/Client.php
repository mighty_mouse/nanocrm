<?php
class Client extends CI_Controller {

        public function __construct(){
            parent::__construct();
            $this->load->model('client_model');
            $this->load->model('externalData_model');
            $this->load->helper('url_helper');
        }

        public function view($id = NULL){
            $data['entity'] = 'client';
            $data['client'] = $this->client_model->get_clients($id);

            if (empty($data['client'])){
                show_404();
            }

            $data['segments'] = $this->externalData_model->get_segments();
            $data['persons'] = $this->externalData_model->get_persons();

            $this->load->view('templates/header', $data);
            $this->load->view('client/view', $data);
            $this->load->view('templates/footer');
        }

        public function index(){
            $data['entity'] = 'client';
            $data['clients'] = $this->client_model->get_clients();
            $data['title'] = 'Clientes';

            $this->load->view('templates/header', $data);
            $this->load->view('client/index', $data);
            $this->load->view('templates/footer');
        }

        public function get(){
          $this->output->set_content_type('application/json');
          $response = $this->client_model->get_clients_by($this->input->post('order_by_field'));
          echo json_encode($response);
        }

        public function create(){
            $data['entity'] = 'client';
            $data['segments'] = $this->externalData_model->get_segments();
            $data['persons'] = $this->externalData_model->get_persons();

            $this->load->view('templates/header', $data);
            $this->load->view('client/create');
            $this->load->view('templates/footer');
        }

        public function edit($id = NULL){
            $data['entity'] = 'client';
            $data['client'] = $this->client_model->get_clients($id);

            if (empty($data['client'])){
                show_404();
            }

            $data['segments'] = $this->externalData_model->get_segments();
            $data['persons'] = $this->externalData_model->get_persons();

            $this->load->view('templates/header', $data);
            $this->load->view('client/edit', $data);
            $this->load->view('templates/footer');
        }

        public function submit(){
            $this->load->helper('form');
            $this->load->library('form_validation');

            $this->form_validation->set_rules('company', 'Empresa', 'required');
            $this->form_validation->set_rules('ref', 'Referência', 'required');

            $this->output->set_content_type('application/json');

            if ($this->form_validation->run() == FALSE){
              $response = array('id' => -1, 'msg' => 'Campo(s) inválido(s) ou em falta');
            }else{
              $response = $this->client_model->set_client();
            }

            echo json_encode($response);
        }

        public function delete(){
            $this->output->set_content_type('application/json');
            $response = $this->client_model->delete_client();
            echo json_encode($response);
        }
}
