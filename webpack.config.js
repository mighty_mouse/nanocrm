var webpack = require('webpack');
var path = require('path');

var APP_DIR = path.resolve(__dirname, 'src/app');
var BUILD_DIR = path.resolve(__dirname, 'src/public');
var NODES_DIR = path.resolve(__dirname, 'node_modules');
var LIBS_DIR = 'src/lib';

var config = {
  entry: {
    //multiple entry points, 1 for each view
    client_index: APP_DIR+'/client/index.jsx',
    client_view: APP_DIR+'/client/view.jsx',
    client_create: APP_DIR+'/client/create.jsx',
    client_edit: APP_DIR+'/client/edit.jsx',
    person_index: APP_DIR+'/person/index.jsx',
    person_view: APP_DIR+'/person/view.jsx',
    person_create: APP_DIR+'/person/create.jsx',
    person_edit: APP_DIR+'/person/edit.jsx',
    // Since react is installed as a node module, node_modules/react,
    // we can point to it directly, just like require('react');
    vendors: [
      'react'
    ]
  },
  output: {
    path: BUILD_DIR,
    filename: '[name].bundle.js'
  },
  externals: {
      // require("jquery") is external and available on the global var jQuery
      "jquery": "jQuery"
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        exclude: [NODES_DIR],
        loaders : ['babel']
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
  ]
};

module.exports = config;
